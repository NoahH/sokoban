document.addEventListener('keydown', movePlayer);
        const map = [  
            "    WWWWW          ",  
            "    W   W          ",  
            "    WB  W          ",  
            "  WWW  BWW         ",  
            "  W  B B W         ",  
            "WWW W WW W   WWWWWW",  
            "W   W WW WWWWW  OOW",  
            "W B  B          OOW",  
            "WWWWW WWW WSWW  OOW",  
            "    W     WWWWWWWWW",  
            "    WWWWWWW        "  
        ]
        let instanceMap = [];
        let playerRow = -1, playerCol = -1;
        createBoard();
        createMap();
        function createMap(){
            for(let i = 0; i < map.length; i ++){
                instanceMap[i] = (map[i].split(""));
            }
        }
        function createBoard(){
            /*for(let i = 0; i < map.length; i ++){
                let temp = document.createElement("div");
                temp.class = "row";
                temp.id = "row" + i;
                for(let j = 0;  j < map[i].length; j ++){
                    let cell = document.createElement("div");
                    cell.class = "cell";
                    cell.id = "cellr" + i + "c" + j;
                    cell.innerHTML = map[i][j];
                    if(map[i][j] == " ")
                        cell.innerHTML = '&nbsp;';
                    if(map[i][j] == "S"){
                        playerCol = j;
                        playerRow = i;
                        map[i][j] = "O";
                    }
                    temp.appendChild(cell);
                }
                document.getElementById("gameBoard").appendChild(temp);
            }*/
            for(let j = 0; j < map[0].length; j ++){
                let temp = document.createElement("div");
                temp.class = "row";
                temp.id = "row" + j;
                for(let i = 0 ; i < map.length; i ++){
                    let cell = document.createElement("div");
                    cell.class = "cell";
                    cell.id = "cellr" + i + "c" + j;
                    cell.innerHTML = map[i][j];
                    if(map[i][j] == " ")
                        cell.innerHTML = "\u00A0";
                    else if(map[i][j] == "S"){
                        playerCol = j;
                        playerRow = i;
                    }
                    temp.appendChild(cell);
                }
                document.getElementById("gameBoard").appendChild(temp);
            }
        }
        function checkWin(){
            let numO = 0, numX = 0;
            for(let i = 0; i < instanceMap.length; i ++){
                var line = instanceMap[0];
                for(let j = 0; j < line.length; j ++){
                    if(instanceMap[i][j] === "O")
                        return false;
                    else if(instanceMap[i][j] === "X")
                        numX ++;
                }
                for(let j = 0; j < line.length; j ++){
                    if(map[i][j] === "O")
                        numO ++;
                    else if(map[i][j] === "X")
                        numO ++;
                }
            }
            if(numO == numX)
                return true;
            else
                return false;
        }
        function addActualSpace(tile){
            if(tile === " ")
                return "\u00A0";
            return tile;
        }
        function checkBoard(){
            let ret = map[playerRow][playerCol];
            if(ret == "B")
                return "\u00A0";
            else if(ret == "X")
                return "O";
            return ret;
        }
        function updateBoard(){
            for(let i = 0; i < instanceMap.length; i ++){
                var line = instanceMap[0];
                for(let j = 0; j < line.length; j ++){
                    if(instanceMap[i][j] === "B" && map[i][j] === "O"){
                        instanceMap[i][j] = "X";
                        document.getElementById("cellr" + i + "c" + j).innerHTML = "X";
                    }
                }
            }
        }
        function movePlayer(e){
            let temp = "";
            if(e.key == "ArrowDown"){
                if(instanceMap[playerRow + 1][playerCol] != "W"){
                    if(instanceMap[playerRow + 1][playerCol] != "B" && instanceMap[playerRow + 1][playerCol] != "X"){
                        document.getElementById("cellr" + (playerRow + 1) + "c" + playerCol).innerHTML = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerRow ++;
                    }else if((instanceMap[playerRow + 1][playerCol] == "B" || instanceMap[playerRow + 1][playerCol] == "X") && (instanceMap[playerRow + 2][playerCol] != "B" && instanceMap[playerRow + 2][playerCol] != "X" && instanceMap[playerRow + 2][playerCol] != "W")){
                        document.getElementById("cellr" + (playerRow + 2) + "c" + playerCol).innerHTML = "B";
                        document.getElementById("cellr" + (playerRow + 1) + "c" + playerCol).innerHTML = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow + 2][playerCol] = "B";
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerRow ++;
                    }
                }
            }
            else if(e.key == "ArrowLeft"){
                if(instanceMap[playerRow][playerCol - 1] != "W"){
                    if(instanceMap[playerRow][playerCol - 1] != "B" && instanceMap[playerRow][playerCol - 1] != "X"){
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol - 1)).innerHTML = "I";
                        instanceMap[playerRow][playerCol - 1] = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerCol --;
                    }else if((instanceMap[playerRow][playerCol - 1] == "B" || instanceMap[playerRow][playerCol - 1] == "X") && (instanceMap[playerRow][playerCol - 2] != "B" && instanceMap[playerRow][playerCol - 2] != "W" && instanceMap[playerRow][playerCol - 2] != "X")){
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol - 2)).innerHTML = "B";
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol - 1)).innerHTML = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol - 2] = "B";
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerCol --;
                    }
                }
            }
            else if(e.key == "ArrowRight"){
                if(instanceMap[playerRow][playerCol + 1] != "W"){
                    if(instanceMap[playerRow][playerCol + 1] != "B" && instanceMap[playerRow][playerCol + 1] != "X"){
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol + 1)).innerHTML = "I";
                        instanceMap[playerRow][playerCol + 1] = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerCol ++;
                    }else if((instanceMap[playerRow][playerCol + 1] == "B" || instanceMap[playerRow][playerCol + 1] == "X") && (instanceMap[playerRow][playerCol + 2] != "B" && instanceMap[playerRow][playerCol + 2] != "W" && instanceMap[playerRow][playerCol + 2] != "X")){
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol + 2)).innerHTML = "B";
                        document.getElementById("cellr" + (playerRow) + "c" + (playerCol + 1)).innerHTML = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol + 2] = "B";
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerCol ++;
                    }
                }
            }
            else if(e.key == "ArrowUp"){
                if(instanceMap[playerRow - 1][playerCol] != "W"){
                    if(instanceMap[playerRow-1][playerCol] != "B" && instanceMap[playerRow-1][playerCol] != "X"){
                        document.getElementById("cellr" + (playerRow - 1) + "c" + playerCol).innerHTML = "I";
                        instanceMap[playerRow - 1][playerCol] = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerRow --;
                    }else if((instanceMap[playerRow - 1][playerCol] == "B" || instanceMap[playerRow - 1][playerCol] == "X") && (instanceMap[playerRow - 2][playerCol] != "X" && instanceMap[playerRow - 2][playerCol] != "B" && instanceMap[playerRow - 2][playerCol] != "W")){
                        document.getElementById("cellr" + (playerRow - 2) + "c" + playerCol).innerHTML = "B";
                        document.getElementById("cellr" + (playerRow - 1) + "c" + playerCol).innerHTML = "I";
                        document.getElementById("cellr" + (playerRow) + "c" + playerCol).innerHTML = addActualSpace(checkBoard());
                        instanceMap[playerRow - 2][playerCol] = "B";
                        instanceMap[playerRow][playerCol] = addActualSpace(checkBoard());
                        playerRow --;
                    }
                }
            }
            updateBoard();
            if(checkWin()){
                document.getElementById("gameBoard").innerHTML = "";
                let temp = document.createElement("h1");
                temp.innerHTML = "!!!YOU WIN!!!"
                document.getElementById("gameBoard").appendChild(temp);
            }
        }